# Secure ElasticSearch

Creates a bunch of secured elastic search nodes, either locally or on Digital Ocean. Includes default config to start *two* local ElasticSearch nodes that:

* Use 512Mb RAM each
* Communicate with each other over a private network
* Are accessible behind an SSL layer signed by the default test Jetty self-signed SSL certificate
* Require authentication via basic auth (which is protected by the SSL layer)
* Use Jetty as their webserver
* Have the "head" ElasticSearch plugin installed

## Quickstart

If you really don't want to do anything else, and just want to see a couple of nodes being started up, just check out the code, and then run the following:

    vagrant up

A few minutes later you will have two vagrant boxes running, elasticsearch0 and elasticsearch1. To connect to elasticseach0, load the following in your browser:

* https://33.33.33.51:9443/_plugin/head/

Because the SSL certificate is self signed, you'll have to accept it, and then use the credentials from [realm.properties](config/etc/elasticsearch/realm.properties) - `user/Passw0rd`.

You should see two nodes presented to you on the webpage that's displayed.

## Firing up some nodes properly

But of course, you don't just want to use the defaults that come with this repo, that would be silly. Instead, you want to provide your own. To do that, copy the config directory from this repo, and put it somewhere else on your local system. You might want to think about putting it in its own repo.

Anywway, copy the files, and then make whatever edits make sense. At the very least, I would expect that you'd be wanting to change:

* config/etc/elasticsearch/keystore - replace this with your own keystore, see below.
* config/etc/elasticsearch/realm.properties - usernames and passwords that can connect to your nodes.
* config/hieradata/common.yaml
    * cluster.name - Set a cluster name that makes sense for your purposes. This wants to be unique, as otherwise other nodes you don't expect might connect to you.
    * keystore_password - The password to the keystore you just created.

Once you've changed what you want to change, you're ready to fire up your nodes.

### For Development

#### Locally

To run the nodes locally, all you need to do it tell vagrant where to find your config:

    ES_SETUP_CONFIG_PATH=../PATH/TO/CONFIG vagrant up

If you later need to reprovision your box, you'll need to remember to pass in your config again:

    ES_SETUP_CONFIG_PATH=../PATH/TO/CONFIG vagrant provision

Now that you potentially have more than one node, you'll need to refer to the specific one you want if you want to SSH in:

    vagrant ssh elasticsearch0

#### On Digital Ocean

1. Make sure you have the [Digital Ocean Vagrant plugin](https://github.com/smdahlen/vagrant-digitalocean) installed on your local mchine.
2. Have your Digital Ocean API keys handy. Currently the script is only dealing with v1 keys.
3. Run the extended `vagrant up` command.

```bash
ES_SETUP_CONFIG_PATH=../PATH/TO/CONFIG DO_CLIENT_ID='YOUR_CLIENT_ID' DO_API_KEY='YOUR_API_KEY' vagrant up --provider=digital_ocean
```

NB This is not the preferred way to fire up nodes for live. This is so that you can create a dev environment off your local machine if you want to.

#### Command line options

These options have to be set as environment variables before running Vagrant, because that's how Vagrant works.

* `ES_SETUP_CONFIG_PATH`: the directory on your local machine where you store your vagrant.yml and hiera setup files. This allows you to store this setup outside of this repo, in its own lovely version controlled place.
* `DO_CLIENT_ID`: Your Digital Ocean client ID for setting up nodes on Digital Ocean.
* `DO_API_KEY`: Your Digital Ocean API key for setting up nodes on Digital Ocean.

### Deploying to production environments

On your machine, check out this repository where you'd like it to live. Then run:

    ./scripts/bootstrap-production bitbucket /path/on/bitbucket/to/config/repo.git

See the [production.yaml](config/hieradata/environment.yaml) file for example
config that you may want to set.

## User Configuration

The following contain sensible defaults. To override them, please create the same data structure elsewhere, and refer to it using the `ES_SETUP_CONFIG_PATH` environment variable.

* [config/](config/)
    * [vagrant.yaml](config/vagrant.yaml) - Basic settings for vagrant,
    including how many nodes you want setting up, and how much RAM they should
    have.
    * [hiera.yaml](config/hiera.yaml) - Basic hiera settings. This is where
    you'll set up your hierarchy if you have one.
    * [hieradata/](config/hieradata)
        * [common.yaml](config/hieradata/common.yaml) - Parameters to be passed
        to your ElasticSearch nodes, including all ElasticSearch setup.
    * [etc/elasticsearch](config/etc/elasticsearch)
        * [keystore](config/etc/elasticsearch/keystore) - An SSL certificate,
        generated using [keytool](http://wiki.eclipse.org/Jetty/Howto/Configure_SSL#Generating_Keys_and_Certificates_with_JDK_keytool)
        * [realm.properties](config/etc/elasticsearch/realm.properties) - A set
        of usernames and (potentially obfuscated) passwords to use to connect to
        your ElasticSearch nodes behind SSL.

This config directory will end up living in `/etc/secureelasticsearch/` as a result of instantiating the Vagrant box, which is why the `datadir` pointer in [hiera.yaml](config/hiera.yaml) points to this location.

## Running tests

There are a few tests currently, for the Puppet custom functions that are written in ruby. To run them, run the following command:

    vagrant ssh elasticsearch0 -c "cd /home/vagrant/secureelasticsearch/puppet/local-modules/securedelasticsearch && rake spec"
