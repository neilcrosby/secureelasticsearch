#! /usr/bin/python
# -*- coding: utf-8 -*-

from datetime import datetime
from elasticsearch import Elasticsearch
from elasticsearch import helpers
import elasticsearch
import argparse
import json
import warnings
from time import sleep

# Deliberately suppress InsecureRequestWarning so that we don't get extra
# output for self signed certificated servers like ours.
import urllib3
urllib3.disable_warnings()


def _get_first(iterable, default=None):
    if iterable:
        for item in iterable:
            return item
    return default


def _get_args_from_commandline():
    parser = argparse.ArgumentParser(
        description='Re-index from one elasticsearch index to another, ' +
                    'moving aliases at the same time.')
    parser.add_argument(
        'to_index',
        help="The new index to copy data to")
    parser.add_argument(
        '--to_index_json',
        default='{}',
        help="The JSON object to use to generate the new index")
    parser.add_argument(
        '--host',
        default='127.0.0.1', required=False,
        help="The IP address or hostname of the node to be contacted")
    parser.add_argument(
        '--port',
        default=9443, required=False, type=int,
        help="The port to connect to the host with")
    parser.add_argument(
        '--user',
        required=False,
        help="Basic Auth username")
    parser.add_argument(
        '--password',
        required=False,
        help="Basic Auth password")
    parser.add_argument(
        '--delete_replaced_indices',
        required=False)

    args = parser.parse_args()

    args.to_index.strip()
    args.host.strip()
    args.user.strip()

    return args


def _get_client(host, port, user, password):
    with warnings.catch_warnings():
        # Ignore warnings during instantiation of ElasticSearch client,
        # because we know we're connecting to a self signed host
        warnings.simplefilter("ignore")

        client = Elasticsearch(
            [
                {
                    'host': args.host,
                    'port': args.port,
                    'use_ssl': True,
                    'http_auth': "{}:{}".format(args.user, args.password),
                    'verify': False,
                },
            ],
        )

    # First, try to connect to the elasticsearch node.
    # If we can't do this then there's no earthly point continuing.
    try:
        client.info()
    except elasticsearch.exceptions.TransportError:
        return None

    return client


def _have_shards_completed_recovery(client):
    """
    Return True if all shards have completed recovery.

    Once all shards have completed recovery, it's safe to start running
    queries against our indices.
    """
    response = client.indices.recovery(index='_all')

    return all(
        (
            'DONE' == shard.get('stage', None)
            for shard in item.get('shards')
        )
        for item in response.itervalues()
    )


def _is_config_changed(client, alias, config):
    # Get the metadata for the alias we're using. We need to check whether the
    # current metadata is the same as the metadata we've been passed. If they
    # are the same, then we should get out of here because there's nothing for
    # us to do.
    # http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/cluster-state.html
    #
    # NB: We're not checking all metadata - we're dropping the 'aliases' and
    # 'state' sections, as well as the 'uuid' and 'version' keys from the
    # 'settings' section.
    state = client.cluster.state(metric='metadata', index=alias)
    indices = state['metadata']['indices']
    existing_index_meta = {}

    if len(indices) > 0:
        found_index_name = _get_first(indices.keys())
        del indices[found_index_name]['state']
        del indices[found_index_name]['aliases']
        del indices[found_index_name]['settings']['index']['uuid']
        del indices[found_index_name]['settings']['index']['version']
        del indices[found_index_name]['settings']['index']['creation_date']

        default_mapping = indices[found_index_name]['mappings'].get(
            '_default_', None
        )

        if default_mapping:
            # Remove empty properties section from _default_ mapping
            if default_mapping.get('properties', None) == {}:
                del default_mapping['properties']

            # Remove any sections from mappings which are the same as those
            # in the default
            for key in indices[found_index_name]['mappings']:
                if key == '_default_':
                    continue

                mapping = indices[found_index_name]['mappings'][key]

                for sub_key in default_mapping:
                    print sub_key
                    if mapping[sub_key] == default_mapping[sub_key]:
                        print "DELETING SUB KEY %s" % sub_key
                        del mapping[sub_key]

                    default_props = default_mapping.get('properties', {})
                    properties = mapping.get('properties', {})

                    for prop_key in default_props:
                        if default_props[prop_key] == properties.get(prop_key, {}):
                            del properties[prop_key]

        existing_index_meta = indices[found_index_name]

    # Make dummies for the keys that ElasticSearch will always return if
    # they're not in to_index_meta already
    to_index_meta = json.loads(config)
    if 'settings' not in to_index_meta.keys():
        to_index_meta['settings'] = {
            "index": {
                "number_of_replicas": "1",
                "number_of_shards": "5",
            }
        }

    if 'mappings' not in to_index_meta.keys():
        to_index_meta['mappings'] = {}

    # If the passed in metadata is the same as ElasticSearch knows about, get
    # out of here!
    if to_index_meta == existing_index_meta:
        return False

    return True


def _reindex_index_with_config(
    client, alias, to_index, to_index_json, delete_replaced_indices
):
    # Append a timestamp onto the to_index, to make sure that it's unique
    d = datetime.now()
    to_index += '-' + d.strftime("%Y-%m-%d-%H-%M-%S")

    # Create the to_index if it doesn't exist yet
    if not client.indices.exists(index=to_index):
        client.indices.create(index=to_index, body=to_index_json)

    # Create the alias, pointing at to_index if it doesn't exist yet
    if not client.indices.exists_alias(name=alias):
        client.indices.put_alias(index=to_index, name=alias)
    else:
        # Work out where the alias is currently pointing
        response = client.indices.get_alias(name=alias)
        from_index = _get_first(response.keys())

        # Re-index the data from the old index to the new
        helpers.reindex(
            client=client, source_index=from_index, target_index=to_index
        )

        # Now reindex anything that came in after the first reindex started
        d2 = datetime.now()
        helpers.reindex(
            client=client, source_index=from_index, target_index=to_index,
            query={"query": {"range": {"date_modified": {"gte":  d}}}}
        )

        # Point the alias at the new index
        client.indices.update_aliases(
            body={
                "actions": [
                    {
                        "remove": {
                            "index": from_index,
                            "alias": alias
                        }
                    },
                    {
                        "add": {
                            "index": to_index,
                            "alias": alias
                        }
                    }
                ]
            }
        )

        # Copy over anything that happened to still have been changed since
        # the second reindex! This is the last time we need to do this,
        # because we've swapped the alias over already
        helpers.reindex(
            client=client, source_index=from_index, target_index=to_index,
            query={"query": {"range": {"date_modified": {"gte":  d2}}}}
        )

        # And delete any trace of the old index if nothing else is pointing at it
        if (from_index and delete_replaced_indices and
           not client.indices.exists_alias(name="*", index=from_index)):
            client.indices.delete(index=from_index)

if __name__ == '__main__':
    args = _get_args_from_commandline()

    client = _get_client(
        host=args.host,
        port=args.port,
        user=args.user,
        password=args.password,
    )

    if not client:
        print "Could not connect to Elasticsearch node! Exiting."
        exit(1)

    max_shard_wait = 10
    while(not _have_shards_completed_recovery(client)):
        max_shard_wait = max_shard_wait - 1
        if max_shard_wait == 0:
            print "Shards did not start in time! Exiting."
            exit(1)

        sleep(1)

    if not _is_config_changed(
        client=client,
        alias=args.to_index,
        config=args.to_index_json
    ):
        exit(2)

    _reindex_index_with_config(
        client=client,
        alias=args.to_index,
        to_index=args.to_index,
        to_index_json=args.to_index_json,
        delete_replaced_indices=int(args.delete_replaced_indices)
    )
