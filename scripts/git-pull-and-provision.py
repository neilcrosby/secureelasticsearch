#! /usr/bin/python
# -*- coding: utf-8 -*-

import argparse
import json
import subprocess
import os
import re


def _get_args_from_commandline():
    parser = argparse.ArgumentParser(
        description='Check a series of directories to see if they can be ' +
                    'updated via git. Either straight from HEAD, or by tag')

    parser.add_argument(
        '--json',
        default='{}', type=json.loads,
        help='A dictionary, keyed by directory name describing which ' +
             'directories to check, and how to check them')

    parser.add_argument(
        '--environment',
        default='production', required=False)

    parser.add_argument(
        '--project_directory',
        default='/home/vagrant/secureelasticsearch', required=False)

    args = parser.parse_args()

    return args


def _get_pipe_to_dev_null():
    return open(os.devnull, 'w')


def _directory_contains_cloned_git_repo(directory):
    return os.path.exists(directory+'/.git')


def _is_new_code_available(directory):
    output = subprocess.check_output(
        """
            cd {directory}

            ssh-agent bash -c 'ssh-add /etc/ssh/git_deployment_rsa_key; \
                git fetch origin'

            LOCAL=$(git rev-parse @)
            REMOTE=$(git rev-parse @{{u}})
            BASE=$(git merge-base @ @{{u}})

            PULL_REQUIRED=0
            if [ $LOCAL = $REMOTE ]; then
                # echo "Up-to-date"
                :
            elif [ $LOCAL = $BASE ]; then
                # echo "Need to pull"
                PULL_REQUIRED=1
            elif [ $REMOTE = $BASE ]; then
                # echo "Need to push"
                :
            else
                # echo "Diverged"
                :
            fi

            echo $PULL_REQUIRED
        """.format(directory=directory),
        shell=True, stderr=_get_pipe_to_dev_null()
    )

    return int(output)


def _get_current_tag(directory):
    tag = subprocess.check_output(
        """
            cd {directory}

            echo "$( git name-rev --tags --name-only $(git rev-parse HEAD) )"
        """.format(directory=directory),
        shell=True, stderr=_get_pipe_to_dev_null()
    )

    # The output we're expecting from the above command is something like
    # v0.1.3^0. We want only the tag name, ie the bit before the caret. So,
    # lets grab it, with a regular expression.

    m = re.search('^([^^]*)', tag)
    if m:
        tag = m.group(1)

    return tag


def _does_tag_exist(directory, options):
    output = subprocess.check_output(
        """
            cd {directory}

            echo "$(
                ssh-agent bash -c 'ssh-add /etc/ssh/git_deployment_rsa_key; \
                    git fetch origin &&
                    git fetch --tags &&
                    git rev-parse {tag} >/dev/null 2>&1 &&
                    echo 1' )"
        """.format(directory=directory, tag=options['tag']),
        shell=True, stderr=_get_pipe_to_dev_null()
    )

    output = output.strip()

    return bool(output)


def _update_from_tag(directory, options):
    output = subprocess.check_output("""
        cd {directory}

        echo "$( git stash && \
            ssh-agent bash -c 'ssh-add /etc/ssh/git_deployment_rsa_key; \
                git fetch origin &&
                git fetch --tags &&
                git checkout {tag}' && \
            git stash pop )"
    """.format(directory=directory, tag=options['tag']), shell=True)

    return output


def _perform_pull(directory):
    output = subprocess.check_output("""
        cd {directory}

        echo "$( git stash && \
            ssh-agent bash -c 'ssh-add /etc/ssh/git_deployment_rsa_key; \
                git pull' && \
            git stash pop )"
    """.format(directory=directory), shell=True)

    return output

if __name__ == '__main__':
    args = _get_args_from_commandline()

    updated = False
    for directory, options in args.json.items():

        if _directory_contains_cloned_git_repo(directory):

            if 'tag' in options.keys():
                if (options['tag'] != _get_current_tag(directory)):
                    if _does_tag_exist(directory, options):
                        print "{directory}: Updating to tag {tag}".format(
                            directory=directory,
                            tag=options['tag']
                        )
                        _update_from_tag(directory, options)
                        updated = True
                    else:
                        print "{directory}: {tag} not found!".format(
                            directory=directory,
                            tag=options['tag']
                        )

            elif _is_new_code_available(directory):
                print "{directory}: Updating from master".format(
                    directory=directory
                )
                print _perform_pull(directory)
                updated = True

    if updated:
        print "Provision starting"

        script = """
            echo "$( {project_dir}/scripts/provision.sh \
                {env} {project_dir} false)"
        """.format(project_dir=args.project_directory, env=args.environment)

        output = subprocess.check_output(script,
                                         shell=True, stderr=subprocess.STDOUT)

        print output
        print "Provision complete"
