#! /bin/bash

DEFAULT_ENVIRONMENT=production
DEFAULT_PROJECT_DIR=/home/vagrant/secureelasticsearch
DEFAULT_KNOWN_HOSTS=/root/.ssh/known_hosts

ENVIRONMENT=${1:-$DEFAULT_ENVIRONMENT}
PROJECT_DIR=${2:-$DEFAULT_PROJECT_DIR}
KNOWN_HOSTS=${3:-$DEFAULT_KNOWN_HOSTS}

HOSTS_TO_KNOW=( github.com bitbucket.org )
APT_PACKAGES_REQUIRED=( linux-headers-$(uname -r) build-essential \
    zlib1g-dev libssl-dev libreadline-gplv2-dev libyaml-dev \
    ruby1.9.3 git rake libaugeas-ruby )

GEMS_REQUIRED=( bundler ruby-shadow deep_merge rspec rspec-puppet )

mkdir -p /root/.ssh \
    && touch $KNOWN_HOSTS \
    && chmod 600 $KNOWN_HOSTS

# Add hosts to known_hosts if they haven't already been added
for HOST in "${HOSTS_TO_KNOW[@]}"
do
	ssh-keygen -H  -F "$HOST" > /dev/null
	if [ $? -ne 0 ];
	then
		echo "Adding $HOST to $KNOWN_HOSTS"
	    ssh-keyscan -H "$HOST" >> $KNOWN_HOSTS 2> /dev/null
	fi
done

# Install apt packages if they haven't already been installed
APT_PACKAGES_TO_INSTALL=()
for PACKAGE in "${APT_PACKAGES_REQUIRED[@]}"
do
	if [ $(dpkg-query -W -f='${Status}' $PACKAGE 2>/dev/null | grep -c "ok installed") -eq 0 ];
	then
	  APT_PACKAGES_TO_INSTALL+=($PACKAGE)
	fi
done

if [ ${#APT_PACKAGES_TO_INSTALL[@]} -gt 0 ];
then
	apt-get -y update
	apt-get -y install ${APT_PACKAGES_TO_INSTALL[*]}
fi

# Be explicit about the puppet version since 4 breaks everything right now
# Previously, we were simply ugrading to the latest version, which took us up
# to 4 when it was released on April 15th 2015. Now, we're sticking with the
# latest 3 for the time being.
gem install json_pure -v '~> 1' --no-ri --no-rdoc
gem install facter -v '2.4.6' --no-ri --no-rdoc
gem install puppet -v '~> 3' --no-ri --no-rdoc
gem install librarian-puppet -v '2.2.3' --no-ri --no-rdoc

# Install gems if they haven't already been installed
GEMS_TO_INSTALL=()
for GEM in "${GEMS_REQUIRED[@]}"
do
	if [ $(gem list $GEM -i | grep -c "true") -eq 0 ];
	then
	  GEMS_TO_INSTALL+=($GEM)
	fi
done

if [ ${#GEMS_TO_INSTALL[@]} -gt 0 ];
then
	gem install ${GEMS_TO_INSTALL[*]} --no-ri --no-rdoc
fi

# And finally, run our provisioners
$PROJECT_DIR/scripts/provision.sh $ENVIRONMENT $PROJECT_DIR
