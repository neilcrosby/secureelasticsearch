#! /bin/bash

NOT_RUNNING=`service elasticsearch status | grep "is not running" | wc -l`

if [ 1 == $NOT_RUNNING ]
	then
	service elasticsearch start
fi
