#! /usr/bin/python
# -*- coding: utf-8 -*-
"""Monitor an ElasticSearch node."""

import warnings
import argparse
import subprocess
from socket import gethostname

from elasticsearch import Elasticsearch
from elasticsearch.exceptions import TransportError

# Deliberately suppress InsecureRequestWarning so that we don't get extra
# output for self signed certificated servers like ours.
import urllib3
urllib3.disable_warnings()


def _get_args_from_commandline():
    parser = argparse.ArgumentParser(
        description='Monitor the ElasticSearch node that lives on the . ' +
                    'specified host. Send an email if it fails.')
    parser.add_argument(
        '--host',
        default='127.0.0.1', required=False,
        help="The IP address or hostname of the node to be contacted")
    parser.add_argument(
        '--port',
        default=9443, required=False, type=int,
        help="The port to connect to the host with")
    parser.add_argument(
        '--user',
        default='',
        required=False,
        help="Basic Auth username")
    parser.add_argument(
        '--password',
        required=False,
        help="Basic Auth password")
    parser.add_argument(
        '--auto_restart',
        action='store_true',
        default=False,
        help="Automatically restart the node if it fails")

    args = parser.parse_args()

    args.host.strip()
    args.user.strip()

    return args


def _get_client(host, port, user, password):
    with warnings.catch_warnings():
        # Ignore warnings during instantiation of ElasticSearch client,
        # because we know we're connecting to a self signed host
        warnings.simplefilter("ignore")

        client = Elasticsearch(
            [
                {
                    'host': args.host,
                    'port': args.port,
                    'use_ssl': True,
                    'http_auth': "{}:{}".format(args.user, args.password),
                    'verify': False,
                },
            ],
        )

    # First, try to connect to the elasticsearch node.
    # If we can't do this then there's no earthly point continuing.
    try:
        client.info()
    except TransportError:
        raise

    return client


def _print_machine_name():
    print "This host: %s" % gethostname()


def _restart_service():
    print
    print "Automatically restarting service"

    command = ['service', 'elasticsearch', 'restart']
    return_code = subprocess.call(command, shell=False)

    if return_code == 0:
        print "Restart completed successfully"
    else:
        print "*** Error restarting ***"

if __name__ == '__main__':
    args = _get_args_from_commandline()

    try:
        client = _get_client(
            host=args.host,
            port=args.port,
            user=args.user,
            password=args.password,
        )
    except TransportError as e:
        _print_machine_name()
        print
        print "Could not connect to Elasticsearch node %s!" % args.host
        print str(e)
        if args.auto_restart:
            _restart_service()

        exit(1)

    health = client.cluster.health()
    status = health.get('status', None)

    if status != 'green':
        _print_machine_name()
        print
        print "ElasticSearch node %s is not green." % args.host
        print "You probably want to do something about that."
        print
        print "Maybe run `service elasticsearch restart` on that box?"
        print
        print health

        if args.auto_restart:
            _restart_service()
