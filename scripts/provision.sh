#! /bin/bash

DEFAULT_ENVIRONMENT=production
DEFAULT_PROJECT_DIR=/home/vagrant/secureelasticsearch
DEFAULT_COLOR=ansi

ENVIRONMENT=${1:-$DEFAULT_ENVIRONMENT}
PROJECT_DIR=${2:-$DEFAULT_PROJECT_DIR}
COLOR=${3:-$DEFAULT_COLOR}

export PATH=$PATH:/usr/local/bin

cd $PROJECT_DIR/puppet && librarian-puppet install

puppet apply --environment=$ENVIRONMENT --color=$COLOR --hiera_config=/etc/secureelasticsearch/hiera.yaml --modulepath=$PROJECT_DIR/puppet/modules/ $PROJECT_DIR/puppet/base.pp
