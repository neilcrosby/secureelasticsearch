#! /bin/bash

if [ "$#" -le 1 ]; then
    echo "Usage: ./bootstrap-production.sh CONFIG_REPO_HOST CONFIG_REPO_PATH [ENVIRONMENT]"
    echo
    echo "eg:"
    echo "	./bootstrap-production.sh bitbucket neilcrosby/some-elasticsearch-config.git"
    echo "	./bootstrap-production.sh bitbucket neilcrosby/some-elasticsearch-config.git production"
    exit
fi

DEFAULT_ENVIRONMENT=production

SCRIPT=$(readlink -f "$0")
SCRIPT_PATH=$(dirname "$SCRIPT")

CONFIG_REPO_HOST=$1
CONFIG_REPO_PATH=$2
ENVIRONMENT=${3:-$DEFAULT_ENVIRONMENT}

if [[ "bitbucket" == "$CONFIG_REPO_HOST" ]]; then
	git clone https://bitbucket.org/$CONFIG_REPO_PATH /etc/secureelasticsearch --depth=5
	cd /etc/secureelasticsearch
	git remote set-url origin git@bitbucket.org:$CONFIG_REPO_PATH
else
	echo "[WARNING] I don't understand '$CONFIG_REPO_HOST' - skipping."
fi

$SCRIPT_PATH/bootstrap.sh $ENVIRONMENT
