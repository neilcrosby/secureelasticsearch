include 'securedelasticsearch'

include 'timezone'

# Set up swap size.
# Due to bug https://github.com/petems/puppet-swap_file/issues/13
# if you want to change swap size, you'll need to run the following on the box
# first before reprovisioning for now:
#   swapoff -a
#   rm /swapfile
#  Alternatively, remove the swapfilesize from config, perform a provision,
#  and then add a new one in again.
$swapfilesize = hiera('swap_file::swapfilesize', 0)
if $swapfilesize != 0 {
  swap_file::files { 'es-swap-file':
    ensure       => present,
    swapfile     => '/swapfile',
    swapfilesize => $swapfilesize
  }
} else {
  swap_file::files { 'es-swap-file':
    ensure   => absent,
    swapfile => '/swapfile',
  }
}

#cron { 'restart-elasticsearch-if-stopped':
#	require => Class['elasticsearch'],
#    ensure  => 'present',
#    command => '/home/vagrant/elasticsearch/scripts/restart-es-if-stopped.sh',
#    user => root,
#    hour => '*/1',
#    minute => '*/1',
#}

define check_directory_presence () {
  exec {"check_directory_presence_${name}":
    command => "/usr/bin/test -e ${name}",
    before => Class["securedelasticsearch"]
  }
}

check_directory_presence { [
  '/home/vagrant/secureelasticsearch/',
  '/etc/secureelasticsearch/',
]: }

resources { "firewall":
  purge => true
}
Firewall {
  before  => Class['my_fw::post'],
  require => Class['my_fw::pre'],
}
class { ['my_fw::pre', 'my_fw::post']: }
class { 'firewall': }

class my_fw::pre {
  Firewall {
    require => undef,
  }

  # Default firewall rules
  firewall { '000 accept all icmp':
    proto   => 'icmp',
    action  => 'accept',
  }->
  firewall { '001 accept all to lo interface':
    proto   => 'all',
    iniface => 'lo',
    action  => 'accept',
  }->
  firewall { '002 accept related established rules':
    proto   => 'all',
    state => ['RELATED', 'ESTABLISHED'],
    action  => 'accept',
  }

    firewall { '100 allow ssh':
        port => '22',
        proto => 'tcp',
        action  => accept,
    }
}

class my_fw::post {
  firewall { '999 drop all':
    proto   => 'all',
    action  => 'drop',
    before  => undef,
    notify  => Service["elasticsearch"],
  }
}
