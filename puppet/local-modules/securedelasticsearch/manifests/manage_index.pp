# Manages an elasticsearch cluster's indexes. Will create indexes and re-index
# when necessary.
define securedelasticsearch::manage_index (
    $all_indices_config,
    $user_credentials,
    $index_name = $name,
    $delete_replaced_indices = true,
    $port = 9443,
) {
    validate_string($index_name)
    validate_hash(
        $all_indices_config,
        $user_credentials,
    )
    validate_bool($delete_replaced_indices)

    $validated_index_config = get_validated_index_config(
        $all_indices_config[$index_name],
        $index_name
    )

    # We pass this through a define so that each alert is output on a new line
    # If you just alert the array, then the whole thing gets blobbed out as one
    # string
    if $validated_index_config["alerts"] {
        manage_index::output_alerts { $validated_index_config["alerts"]: }
    }

    # The index key that we're passed can be a comma separated list. So, we run
    # reindex() once per item in the split list
    $split_index_names = split($index_name, ',')

    manage_index::reindex { $split_index_names:
        validated_index_config  => $validated_index_config,
        delete_replaced_indices => $delete_replaced_indices,
        user_credentials        => $user_credentials,
        port                    => $port,
    }
}
