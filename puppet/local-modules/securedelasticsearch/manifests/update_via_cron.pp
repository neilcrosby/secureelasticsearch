# == Class: securedelasticsearch::update_via_cron
#
# Defines and sets up cron jobs for automatically updating this node
class securedelasticsearch::update_via_cron (
    $update_via_cron = false,
    $cron_hash = {}
) {
    if (!$update_via_cron) {
        cron { 'auto-update-from-git':
            ensure  => 'absent',
        }
    } else {
        validate_hash($cron_hash)
        validate_string($cron_hash['key_name'])
        validate_string($cron_hash['email'])

        $config_dir = '/etc/secureelasticsearch'
        $configurator_dir = '/home/vagrant/secureelasticsearch'
        $script_dir = "${configurator_dir}/scripts"

        include git

        git::config { 'user.name':
          value => 'John Doe',
        }

        git::config { 'user.email':
          value => 'john.doe@example.com',
        }

        class { 'ssh':
          storeconfigs_enabled => true,
        }

        ssh::server::host_key {'git_deployment_rsa_key':
            private_key_source => inline_template("${config_dir}/.ssh/<%= @cron_hash['key_name'] %>"),
            public_key_source  => inline_template("${config_dir}/.ssh/<%= @cron_hash['key_name'] %>.pub"),
        }

        if $cron_hash["schedule"] and $cron_hash["schedule"]["hour"] {
            $hour = $cron_hash['schedule']['hour']
        } else {
            $hour = '*'
        }

        if $cron_hash["schedule"] and $cron_hash["schedule"]["minute"] {
            $minute = $cron_hash['schedule']['minute']
        } else {
            $minute = '*/15'
        }

        # Deliberately not setting the cron_environment variable if we don't
        # have an email to send to. The cron class gets upset if we do.
        if $cron_hash["email"] {
            include sendmail
            $cron_environment = "MAILTO=${cron_hash['email']}"
        }

        if $cron_hash["update_config"] {
            $update_config_hash = { "${config_dir}" => {} }
        } else {
            $update_config_hash = { }
        }

        if $cron_hash['update_configurator'] {
            if $cron_hash['configurator_tag'] {
                $conf_conf = { 'tag' => $cron_hash['configurator_tag'] }
            } else {
                $conf_conf = {}
            }

            $update_configurator_hash = {
                "${configurator_dir}" => $conf_conf
            }
        }

        $cron_json = to_json(
            merge($update_config_hash, $update_configurator_hash)
        )

        cron { 'auto-update-from-git':
            ensure      => 'present',
            command     =>
                "${script_dir}/git-pull-and-provision.py \
                --environment=${::environment} \
                --project_directory=${configurator_dir} \
                --json='${cron_json}'",
            user        => root,
            hour        => $hour,
            minute      => $minute,
            environment => $cron_environment,
            require     => Class['git']
        }
    }
}
