# == Class: securedelasticsearch::update_via_cron
#
# Defines and sets up cron jobs for automatically updating this node
class securedelasticsearch::monitor (
    $monitor = false,
    $monitor_hash = {},
    $user_credentials,
    $port = 9443,
) {
    $cron_job_name = 'securedelasticsearch-monitor'

    if (!$monitor) {
        cron { $cron_job_name:
            ensure  => 'absent',
        }
    } else {
        include sendmail

        validate_hash(
            $monitor_hash,
            $user_credentials,
        )
        validate_string($monitor_hash['email'])

        if $monitor_hash["schedule"] and $monitor_hash["schedule"]["hour"] {
            $hour = $monitor_hash['schedule']['hour']
        } else {
            $hour = '*'
        }

        if $monitor_hash["schedule"] and $monitor_hash["schedule"]["minute"] {
            $minute = $monitor_hash['schedule']['minute']
        } else {
            $minute = '*/5'
        }

        $username = $user_credentials['user']
        $password = $user_credentials['password']

        if $monitor_hash['auto_restart'] {
            $auto_restart = '--auto_restart'
        } else {
            $auto_restart = ''
        }

        $script = "/usr/bin/python \
            /home/vagrant/secureelasticsearch/scripts/monitor.py \
            --port=${port} \
            --user=${username} --password=${password} ${auto_restart}"

        $monitor_environment = "MAILTO=${monitor_hash['email']}"

        cron { $cron_job_name:
            ensure      => 'present',
            command     => $script,
            user        => root,
            hour        => $hour,
            minute      => $minute,
            environment => $monitor_environment,
        }
    }
}