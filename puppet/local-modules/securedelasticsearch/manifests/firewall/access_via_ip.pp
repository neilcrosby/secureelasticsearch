define securedelasticsearch::firewall::access_via_ip ($port) {
    firewall { "300 accept transport access on port ${name}:${port}":
        port   => $port,
        proto  => 'tcp',
        action => 'accept',
        notify => Service['elasticsearch'],
        source => $name,
    }
}

