# == Class: securedelasticsearch
#
# The base class for instantiating a secured elasticsearch node
class securedelasticsearch (
    $elasticSearchVersion = '1.2.1',
    $heapSize = '256m',
    $maxLockedMemory = 'unlimited',
    $maxOpenFiles = 65535,
    $user = 'elasticsearch',
    $group = 'elasticsearch',
    $update_via_cron = false,
    $monitor = false,
    $manage_indices = false,
    $delete_replaced_indices = true,
) {
    # We seem to have to explicitly do this, rather than allowing the configHash
    # to be passed in because there's no automatic way to do deeper merges
    # with puppet and hiera
    # https://docs.puppetlabs.com/hiera/1/lookup_types.html#hash-merge
    # https://projects.puppetlabs.com/issues/20199
    $config_hash = hiera_hash('securedelasticsearch::configHash')
    $cron_hash = hiera_hash('securedelasticsearch::cron_hash', {})
    $monitor_hash = hiera_hash('securedelasticsearch::monitor_hash', {})

    validate_string(
        $elasticSearchVersion,
        $heapSize,
        $user,
        $group,
    )

    validate_bool(
        $update_via_cron,
        $manage_indices,
    )

    validate_hash(
        $config_hash,
        $cron_hash
    )

    $user_credentials = get_user_credentials(
        '/etc/secureelasticsearch/etc/elasticsearch/realm.properties'
    )

    # Increase operating system limits on mmap counts
    # http://www.elastic.co/guide/en/elasticsearch/reference/master/setup-configuration.html#vm-max-map-count
    sysctl { 'vm.max_map_count': value => '262144' }

    # Only allow memory swapping in emergencies
    # http://www.elastic.co/guide/en/elasticsearch/reference/master/setup-configuration.html#setup-configuration-memory
    sysctl { 'vm.swappiness': value => '1' }

    # Set up an elasticsearch user and group explicitly, so that we can use
    # it to own the jetty config directory
    group { 'elasticsearch_group':
        ensure => present,
        name   => $group
    }

    user { 'elasticsearch_user':
        ensure  => present,
        name    => $user,
        require => Group['elasticsearch_group'],
        groups  => [ $group ]
    }

    package { 'openjdk-7-jre-headless':
        ensure => present,
    }

    $defaults_hash = {
        'ES_USER' => $user,
        'ES_GROUP' => $group,
        'ES_HEAP_SIZE' => $heapSize,
        'MAX_LOCKED_MEMORY' => $maxLockedMemory,
        'MAX_OPEN_FILES' => $maxOpenFiles,
    }

    class { 'elasticsearch':
        init_defaults => $defaults_hash,
        config        => $config_hash,
        package_url   => "https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-${elasticSearchVersion}.deb",
        require       => Package['openjdk-7-jre-headless'],
        autoupgrade   => true,
    }

    $realm    = '/etc/secureelasticsearch/etc/elasticsearch/realm.properties'
    $keystore = '/etc/secureelasticsearch/etc/elasticsearch/keystore'

    file {
        '/etc/elasticsearch/realm.properties':
            source => $realm,
            notify => Service['elasticsearch'],
    }

    file {
        '/etc/elasticsearch/keystore':
            source => $keystore,
            notify => Service['elasticsearch'],
    }

    # Create the directory that the jetty plugin tries to put itself in so
    # that it doesn't fail on first provision
    file { [
        '/usr/share/elasticsearch',
        '/usr/share/elasticsearch/config',
        '/usr/share/elasticsearch/config/jetty'
    ]:
        ensure => 'directory',
        owner  => 'elasticsearch',
    }

    elasticsearch::plugin{ 'elasticsearch-jetty':
        module_dir => 'jetty',
        url        => 'https://oss-es-plugins.s3.amazonaws.com/elasticsearch-jetty/elasticsearch-jetty-1.2.1.zip',
        require    => File['/usr/share/elasticsearch/config/jetty'],
        # Once the Jetty plugin has installed, tell the jetty config
        # directory that elasticsearch expects to point at the config
        # directory that's just been added
        notify     => File['/usr/share/elasticsearch/plugins/jetty/config']
    }

    # Link the expected jetty directory to the actual one
    file { '/usr/share/elasticsearch/plugins/jetty/config':
        ensure  => 'link',
        target  => '/usr/share/elasticsearch/config/jetty',
        force   => true,
        require => File['/usr/share/elasticsearch/config/jetty'],
    }

    elasticsearch::plugin{'mobz/elasticsearch-head':
        module_dir => 'head',
    }

    class { 'python':
        version => 'system',
        pip     => true,
    }

    python::pip { 'python-elasticsearch':
        pkgname       => 'elasticsearch',
        ensure        => '1.8.0'
    }

    class { 'securedelasticsearch::firewall':
        config_hash => $config_hash,
    }

    class { 'securedelasticsearch::update_via_cron':
        update_via_cron => $update_via_cron,
        cron_hash       => $cron_hash,
    }

    $jetty_key = 'sonian.elasticsearch.http.jetty'
    if $config_hash[$jetty_key] and $config_hash[$jetty_key]['ssl_port'] {
        $ssl_port = $config_hash[$jetty_key]['ssl_port']
    } else {
        $ssl_port = 9443
    }

    class { 'securedelasticsearch::monitor':
        monitor          => $monitor,
        monitor_hash     => $monitor_hash,
        user_credentials => $user_credentials,
        port             => $ssl_port,
    }

    # We don't use the curl retry functionality, as we get an exit code of 7
    # (CURLE_COULDNT_CONNECT) when the server hasn't yet come up. In this
    # instance, curl does not seem to retry. So, we'll use Puppet's tries and
    # try_sleep to repeat the curl command several times instead.
    $es_user     = $user_credentials['user']
    $es_password = $user_credentials['password']
    exec { 'check_elasticsearch_came_up':
        command   => "/usr/bin/curl \
            -k --fail --user ${es_user}:${es_password} \
            'https://${::ipaddress_eth1}:${ssl_port}/_cluster/health?wait_for_status=yellow&timeout=90s'",
        tries     => 5,
        try_sleep => 5,
        subscribe => Service['elasticsearch'],
    }

    if ($manage_indices) {
        $indices = hiera_hash('securedelasticsearch::indices', {})
        $base_index_names = keys($indices)

        # This calls manage-index multiple times, once for each item in
        # $base_index_names. We have to pass through the whole $indices hash
        # because puppet doesn't know how to just pass one in this case
        securedelasticsearch::manage_index { $base_index_names:
            all_indices_config      => $indices,
            delete_replaced_indices => $delete_replaced_indices,
            user_credentials        => $user_credentials,
            port                    => $ssl_port,
            require                 => Exec['check_elasticsearch_came_up']
        }
    }
}
