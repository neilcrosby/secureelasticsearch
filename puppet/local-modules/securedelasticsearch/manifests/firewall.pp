# == Class: securedelasticsearch::firewall
#
# Sets up firewalls for the secured elasticsearch node
class securedelasticsearch::firewall (
    $config_hash
) {
    if $config_hash["sonian.elasticsearch.http.jetty"] and
        $config_hash["sonian.elasticsearch.http.jetty"]["ssl_port"]
    {
        $ssl_port = $config_hash["sonian.elasticsearch.http.jetty"]["ssl_port"]
    } else {
        $ssl_port = 9443
    }

    if $config_hash["http.port"] {
        $http_port = $config_hash["http.port"]
    } elsif $config_hash["http"] and $config_hash["http"]["port"] {
        $http_port = $config_hash["http"]["port"]
    } else {
        $http_port = 9200
    }

    if $config_hash["transport.tcp.port"] {
        $transport_port = $config_hash["transport.tcp.port"]
    } elsif $config_hash["transport"] and
        $config_hash["transport"]["tcp"] and
        $config_hash["transport"]["tcp"]["port"]
    {
        $transport_port = $config_hash["transport"]["tcp"]["port"]
    } else {
        $transport_port = 9300
    }

    if $config_hash["discovery.zen.ping.multicast.port"] {
        $zen_port = $config_hash["discovery.zen.ping.multicast.port"]
    } else {
        $zen_port = 54328
    }

    firewall { "100 drop HTTP access on port ${http_port}":
        port   => $http_port,
        proto  => tcp,
        action => drop,
        notify => Service['elasticsearch'],
    }

    firewall { "200 accept HTTPS access on port ${ssl_port}":
        port   => $ssl_port,
        proto  => tcp,
        action => accept,
        notify => Service['elasticsearch'],
    }

    # The array of hosts that we know about, so that we can lock down
    # communication between nodes to just those known nodes
    $hosts = get_ip_addresses(
        $config_hash, '/etc/secureelasticsearch/vagrant.yaml'
    )

    firewall::access_via_ip { $hosts:
        port => $transport_port
    }

    firewall { "300 accept zen auto discovery on port ${zen_port}":
        port   => $zen_port,
        proto  => 'udp',
        action => 'accept',
        notify => Service['elasticsearch'],
    }
}
