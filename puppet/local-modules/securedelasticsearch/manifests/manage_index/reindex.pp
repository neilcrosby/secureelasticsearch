# Reindexes a single ElasticSearch index
define securedelasticsearch::manage_index::reindex (
    $validated_index_config,
    $delete_replaced_indices,
    $user_credentials,
    $port,
    $index_name = $name,
) {
    $index_metadata = $validated_index_config["metadata"]

    $index_metadata_json = to_json($index_metadata)
    $delete_replaced_indices_as_num = bool2num($delete_replaced_indices)

    $username = $user_credentials['user']
    $password = $user_credentials['password']

    exec { "re_index_using_python_${name}":
        command => "/usr/bin/python \
            /home/vagrant/secureelasticsearch/scripts/re-index.py \
            ${index_name} \
            --port=${port} \
            --user=${username} --password=${password} \
            --to_index_json='${index_metadata_json}' \
            --delete_replaced_indices=${delete_replaced_indices_as_num}",
        require => [
            Exec['check_elasticsearch_came_up'],
            Python::Pip['python-elasticsearch'],
        ],
        timeout => 0,
        returns => [
            0, # All good
            2, # Config hasn't changed, so doing nothing
        ]
    }
}
