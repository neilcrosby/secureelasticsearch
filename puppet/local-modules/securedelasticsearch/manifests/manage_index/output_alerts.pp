# Outputs an alert to the user. Defined in order that we could pass an array of
# alerts into it and have them all be output
define securedelasticsearch::manage_index::output_alerts () {
    alert($name)
}
