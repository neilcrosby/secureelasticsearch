require 'spec_helper'

def deep_copy(o)
  Marshal.load(Marshal.dump(o))
end

def are_all_leaves_strings(node)
    if node.kind_of? (Hash) or node.kind_of? (Array)
        node.each do |value|
            if not are_all_leaves_strings(value)
                return false
            end
        end
    elsif not node.kind_of? (String)
        return false
    end

    return true
end

def get_perfect_configs()
    return [
        {
            'alias' => 'another_alias',
            'metadata' => {
                'settings' => {
                    'index' => {
                        'number_of_shards' => '3',
                        'number_of_replicas' => '0'
                    }
                }
            }
        },
        {
            'alias' => 'some_alias',
            'metadata' => {
                'mappings' => {
                    'some_type' => {
                        'properties' => {
                            'some_date_property' => {
                                'type' => 'date',
                                'format' => 'dateOptionalTime'
                            }
                        }
                    }
                }
            }
        },
    ]
end

def get_configs_needing_stringifying()
    return [
        {
            'alias' => 'another_alias',
            'metadata' => {
                'settings' => {
                    'index' => {
                        'number_of_shards' => 3,
                        'number_of_replicas' => '0'
                    }
                }
            }
        },
        {
            'alias' => 'another_alias',
            'metadata' => {
                'settings' => {
                    'index' => {
                        'number_of_shards' => '3',
                        'number_of_replicas' => 0
                    }
                }
            }
        },
    ]
end

def get_configs_without_index_level
    return [
        {
            'alias' => 'another_alias',
            'metadata' => {
                'settings' => {
                    'number_of_shards' => '3',
                    'number_of_replicas' => '0'
                }
            }
        },
    ]
end

def get_configs_with_dates_but_no_format
    return [
        {
            'alias' => 'some_alias',
            'metadata' => {
                'mappings' => {
                    'some_type' => {
                        'properties' => {
                            'some_date_property' => {
                                'type' => 'date',
                            }
                        }
                    }
                }
            }
        },
    ]
end

def get_configs_with_known_errors()
    # With comments to describe why they error:
    return [
        # Not a hash
        [ 'something', 'something else'],
        # Not a hash
        'something',
        # Not a hash
        nil,
        # It's an empty hash
        {},
        # Alias is nil
        {
            'alias' => nil,
        },
        # No alias
        {
            'metadata' => {
                'settings' => {
                    'number_of_shards' => '3',
                }
            }
        },
        # Expected stringified integer value doesn't look like an integer
        {
            'alias' => 'some_alias',
            'metadata' => {
                'settings' => {
                    'number_of_shards' => '3.7',
                }
            }
        },
        # Expected stringified integer value doesn't look like an integer
        {
            'alias' => 'some_alias',
            'metadata' => {
                'settings' => {
                    'number_of_shards' => 4.2,
                }
            }
        },
        # Expected stringified integer value doesn't look like an integer
        {
            'alias' => 'some_alias',
            'metadata' => {
                'settings' => {
                    'number_of_shards' => 'fish',
                }
            }
        },
        # Expected stringified integer value doesn't look like an integer
        {
            'alias' => 'some_alias',
            'metadata' => {
                'settings' => {
                    'number_of_replicas' => 4.2,
                }
            }
        },
        # Expected stringified integer value doesn't look like an integer
        {
            'alias' => 'some_alias',
            'metadata' => {
                'settings' => {
                    'number_of_replicas' => 'fish',
                }
            }
        },
    ]
end

def get_all_configs_requiring_changes()
    return (
        get_configs_needing_stringifying() +
        get_configs_without_index_level() +
        get_configs_with_dates_but_no_format
    )
end

def get_all_valid_configs()
    return (
        get_perfect_configs() +
        get_all_configs_requiring_changes()
    )
end

describe 'get_validated_index_config' do
    index_name = 'some_index_name'

    describe 'all valid configs return a hash' do
        get_all_valid_configs().each do |config|
            specify {
                expect( subject.call([config, index_name]) ).to be_a Hash
            }
        end
    end

    describe 'a returned config will return itself if run again' do
        get_all_valid_configs().each do |config|
            specify {
                initial_response_config = subject.call([config, index_name])
                expected_final_response = deep_copy(initial_response_config)

                expect( subject.call([initial_response_config, index_name]) ).to eq(expected_final_response)
            }
        end
    end

    describe 'perfect configs just return themselves' do
        get_perfect_configs().each do |config|
            specify {
                expected_response = deep_copy(config)

                expect( subject.call([config, index_name]) ).to eq(expected_response)
            }
        end
    end

    describe 'configs needing changing gain an alerts key containing alerts array' do
        get_all_configs_requiring_changes().each do |config|
            specify {
                response = subject.call([config, index_name])

                expect( response ).to have_key 'alerts'
                expect( response['alerts'] ).to be_an Array
            }
        end
    end

    describe 'all valid configs return all leaf nodes as strings' do
        get_all_valid_configs().each do |config|
            specify {
                response = subject.call([config, index_name])
                expect( are_all_leaves_strings(response)).to eq(true)
            }
        end
    end

    describe 'configs missing an index level have one inserted' do
        get_configs_without_index_level().each do |config|
            specify {
                expect(config['metadata']['settings']).not_to have_key 'index'

                response = subject.call([config, index_name])

                expect( response ).to include('alerts')
                expect( response['metadata']['settings'] ).to have_key 'index'
            }
        end
    end

    describe 'configs with known errors throw exceptions' do
        get_configs_with_known_errors().each do |config|
            specify {
                expect { subject.call([config, index_name]) }.to raise_error(RuntimeError)
            }
        end
    end

    describe 'date type properties without a format are given format of dateOptionalTime' do
        get_configs_with_dates_but_no_format().each do |config|
            specify {
                expect(config['metadata']['mappings']['some_type']['properties']['some_date_property']).not_to have_key 'format'

                response = subject.call([config, index_name])

                expect( response ).to include('alerts')
                expect( response['metadata']['mappings']['some_type']['properties']['some_date_property'] ).to have_key 'format'
                expect( response['metadata']['mappings']['some_type']['properties']['some_date_property']['format'] ).to eq 'dateOptionalTime'
            }
        end
    end
end