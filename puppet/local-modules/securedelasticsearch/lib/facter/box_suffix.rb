# Sets fact to the group of numbers at the end of the first segment of the
# hostname of the box.
#
# e.g.  ourbox-123 => 123
# 		ourbox123  => 123
# 		our1box23  => 23
# 		ourbox123.example.com => 123
Facter.add('box_suffix') do
	setcode do
		hostname = Facter.value(:hostname)
		Facter::Core::Execution.exec("echo '#{hostname}' | /usr/bin/perl -nle 'm|([0-9]+)(\..+)?$|; print $1'")
	end
end
