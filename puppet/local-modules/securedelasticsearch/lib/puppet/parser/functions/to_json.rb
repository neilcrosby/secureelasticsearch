# From https://gist.github.com/falzm/8575549

require 'json'

def to_json(obj)
  return obj.to_json
end

module Puppet::Parser::Functions
  newfunction(:to_json, :type => :rvalue, :doc => <<-EOS
A tiny wrapper to obj.to_json that we can use from puppet
    EOS
  ) do |arguments|
    raise(Puppet::ParseError, "to_json(): Wrong number of arguments " +
      "given (#{arguments.size} for 1)") if arguments.size != 1

    return to_json(arguments[0])
  end
end
