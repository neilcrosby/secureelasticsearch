require 'yaml'

# Takes as input an index_config_hash, and performs whatever actions are
# necessary to validate that it is both valid, and that if it is used it will
# contain the same values as a call to _cluster/state/metadata/{index}
#
# Exceptions will be raised on unrecoverable errors.
#
# Messages will be added to the index_config_hash['alerts'] array about any
# changes which were made to the index to achieve the parity referred to above.
# You are advised to make those same changes to your input index_config_hash
# to suppress these messages.

class String
  def is_stringed_integer?
    self.to_i.to_s == self
  end
end

def validate_stringed_integer(value, name, alerts)
  if value.is_a? Integer
    value = value.to_s

    alerts.push name + " is an integer - converting to string"
  elsif not value.is_a? String or not value.is_stringed_integer?
    raise name + " is not an integer - " + value.to_s
  end

  return value
end

def get_validated_index_config(index_config_hash, index_name)
  if not index_config_hash.is_a? Hash
    raise index_name + ": index_config_hash is not a hash"
  end

  alerts = index_config_hash['alerts'] ? index_config_hash['alerts'] : []

  if index_config_hash['metadata']
    if index_config_hash['metadata']['settings']
      # If the config doesn't have an "index" within "settings", then we'll
      # add that as an extra layer.  In accordance with
      # http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/indices-create-index.html
      # we don't have to do that to be valid, but doing so means we'll have parity
      # with requests for the index state metadata
      if not index_config_hash['metadata']['settings']['index']
        settings = index_config_hash['metadata']['settings']
        index_config_hash['metadata']['settings'] = { 'index' => settings }

        alerts.push index_name + ": Inserting 'index' layer below 'settings'"
      end

      settings = index_config_hash['metadata']['settings']['index']

      # Because elasticsearch returns strings for numeric inputs like
      # number_of_shards and number_of_replicas, we'll turn them into strings
      # here if they weren't already set up that way
      if settings['number_of_replicas']
        settings['number_of_replicas'] = validate_stringed_integer(
          settings['number_of_replicas'], index_name + ": number_of_replicas", alerts
        )
      end

      if settings['number_of_shards']
        settings['number_of_shards'] = validate_stringed_integer(
          settings['number_of_shards'], index_name + ": number_of_shards", alerts
        )
      end
    end

    if index_config_hash['metadata']['mappings']
      index_config_hash['metadata']['mappings'].each do |mapping_key, value|
        if value['properties']
          value['properties'].each do |property_key, property|
            #   if the property type is date and format hasn't been set, then set it to dateOptionalTime
            if "date" == property["type"] and not property.key?('format')
              property['format'] = 'dateOptionalTime'

              alerts.push index_name + ": found a date type without a format - setting to default of 'dateOptionalTime'"
            end
          end
        end
      end
    end
  end

  if alerts.length > 0
    index_config_hash['alerts'] = alerts
  end

  return index_config_hash
end

module Puppet::Parser::Functions
  newfunction(:get_validated_index_config, :type => :rvalue, :doc => <<-EOS
Returns validated index config hash, along with an extra "alerts" key. Throws
exceptions if any data is non-recoverably bad.
    EOS
  ) do |arguments|
    raise(Puppet::ParseError, "get_validated_index_config(): Wrong number of arguments " +
      "given (#{arguments.size}, required 2)") if arguments.size != 2

    return get_validated_index_config(arguments[0], arguments[1])
  end
end