require 'yaml'

# Returns a hash, with keys of 'user' and 'password'.
#
# We're running free and loose here, and are assuming that the file we've
# been passed actually contains some username/password information
def get_user_credentials(user_file)
  user_details = YAML.load_file user_file

  usernames = user_details.keys

  username = usernames[0]

  sub_details = user_details[username].split(",")
  password = sub_details[0]

  credentials = {
    'user' => username,
    'password' => password,
  }

  return credentials
end

module Puppet::Parser::Functions
  newfunction(:get_user_credentials, :type => :rvalue, :doc => <<-EOS
Returns a hash, with keys of 'user' and 'password'.
    EOS
  ) do |arguments|
    raise(Puppet::ParseError, "get_user_credentials(): Wrong number of arguments " +
      "given (#{arguments.size}, expected 1)") if arguments.size != 1

    return get_user_credentials(arguments[0])
  end
end
