require 'yaml'

def get_ip_addresses(config_hash, vagrant_file)
  if (config_hash["discovery"] &&
     config_hash["discovery"]["zen"] &&
     config_hash["discovery"]["zen"]["ping"] &&
     config_hash["discovery"]["zen"]["ping"]["unicast"] &&
     config_hash["discovery"]["zen"]["ping"]["unicast"]["hosts"])

    hosts = config_hash["discovery"]["zen"]["ping"]["unicast"]["hosts"]
  else
    vagrant = YAML.load_file vagrant_file

    if vagrant["base"] && vagrant["base"]["ip-addresses"]
      hosts = vagrant["base"]["ip-addresses"]
    else
      hosts = []
    end
  end

  return hosts
end

module Puppet::Parser::Functions
  newfunction(:get_ip_addresses, :type => :rvalue, :doc => <<-EOS
Returns all known IP addresses in this cluster
    EOS
  ) do |arguments|
    raise(Puppet::ParseError, "get_ip_addresses(): Wrong number of arguments " +
      "given (#{arguments.size} for 2)") if arguments.size != 2

    return get_ip_addresses(arguments[0], arguments[1])
  end
end