# Changes

## 0.16.4 (2016-03-09)

* Make sure we actually reindex things that have changed since the reindexing
  started.

## 0.16.3 (2016-03-09)

* Lock python elasticsearch version to 1.8.0 so that we don't get the 2.0
  version which breaks everything
* Stop whining out warnings whilst we're monitoring
* Fix so that provision doesn't error when no indexes exist yet
* Get rid of temporary hack that was causing restart, that should have been
  got rid of a long time ago

## 0.16.2 (2015-10-09)

* Update check for index equivalency to take into account the _default_ mapping

## 0.16.1 (2015-08-07)

* Don't timeout during reindexing.

## 0.16.0 (2015-08-03)

* Add petems/swap_file and use it to generate swap files if specified in config

## 0.15.1 (2015-04-16)

* Don't allow use of Puppet 4 yet - everything broke.
* Properly wait for shards to come back up before reindexing.

## 0.15.0 (2015-04-02)

* Memory improvements - make servers actually use mlockall: True.

## 0.14.2 (2015-03-02)

* Wait for at least a yellow status before reindexing.

## 0.14.2 (2015-03-02)

* Don't fail `vagrant provision` due to warnings about self signed
  certificates.

## 0.14.0 (2015-01-30)

* Monitoriing improvements:
  * Make schedule configurable
  * Allow elasticsearch to be automatically restarted when there are problems

## 0.13.3 (2014-12-01)

* Make sure we actually check out new tags when we're meant to.

## 0.13.2 (2014-11-10)

* Explicitly add libageas-ruby as provision was failing on live boxes

## 0.13.1 (2014-11-10)

* Make ES_HEAP_SIZE be picked up on 'service elasticsearch restart'

## 0.13.0 (2014-11-06)

* Add simple monitoring.
* Increase ElasticSearch version to 1.3.4

## 0.12.0 (2014-09-16)

* Allow the index key in our config to take multiple, comma separated values.

## 0.11.3 (2014-09-15)

* BUGFIX: Deal with non-existent tags gracefully
* Update cron email output so that it's easier to read

## 0.11.2 (2014-09-08)

* BUGFIX: Push STDERR to /dev/null for the two "checking" actions in
  git-pull-and-provision.py so that they don't spam email on every check.

## 0.11.1 (2014-09-08)

* Allow other environment types on production style boxes to be easily
  provisioned via parameter on bootstrap-production.sh

## 0.11.0 (2014-09-08)

* Update cron configuration to allow configurator to be locked to a specific
  version.
* Set hostname as fully qualified domain name so that emails don't get marked
  as spam.

## 0.10.3 (2014-09-04)

* BUGFIX: Pass host through to re-index.py so that we can deal with non
  standard SSL ports
* Start running linters against python and puppet files

## 0.10.2 (2014-09-02)

* Make scripts/bootstrap-production.sh know about the full directory path that
  it lives in
* Remove unused test.sh

## 0.10.1 (2014-09-02)

* Fix file permissions on scripts/bootstrap-production.sh

## 0.10.0 (2014-09-02)

* Re-integrate create-indices feature branch.
* Allow indices to be created and aliased from heira config.
* Allow indices to be re-indexed when config changes, on reprovision.
* Indices are NOT re-indexed on reprovision when config has not changed.
* Effort is made to validate the indexing config before applying it.

## 0.9.0

* Speed up re-provisioning massively, by not having to go to the network for
  things that we've already installed
* BUGFIX: no more duplicate hosts in the known_hosts file

## 0.8.0

* Only allow nodes with known IP addresses to communicate with each other
* hiera - Allow for box specific config to be loaded based on the numerical
  suffix of the box

## 0.7.0

* Store the last passed in environment variables in a temporary file so that
  the next time you run a vagrant command they'll be automagically picked up
  and used

## 0.6.0

* Allow configurator and/or external config to be updated via a cron job, and
  then reprovisioned

## 0.5.0

* Allow for hiera hierarchy based on ENVIRONMENT type, which can be passed in
  on `vagrant up`
* Allow for setting up a production box simply by running
  `scripts/bootstrap.sh`

## 0.4.1

* Allow zen auto discovery through the firewall so that nodes can discover
  each other
* Auto restart elasticsearch when the firewall changes (because it means that
  ports have changed)
* Extra documentation
* Updated default cluster name

## 0.4.0

* Lock down ports so that it's no longer possible to communicate via HTTP,
  only via HTTPS

## 0.3.0

* Swapped nginx for jetty
* Pull all setup config from a collection of yaml files in `config` directory
* Allow `config` directory to be pulled in from elsewhere
* Allow for basic auth and SSL

## 0.2.0

* Added nginx and basic auth
