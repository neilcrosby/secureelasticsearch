require 'yaml'
require 'pp'

env_config_file = "env.yaml"

env_config = Hash.new
env_config['path'] = 'config'
env_config['environment'] = ENV['ENVIRONMENT'] ? ENV['ENVIRONMENT'] : 'development'
env_config['do_client_id'] = ''
env_config['do_api_key'] = ''

if File.exist?(env_config_file)
    saved_env_config = YAML.load_file env_config_file
    env_config = env_config.merge(saved_env_config)
end

env_config['path'] = ENV['ES_SETUP_CONFIG_PATH'] ? ENV['ES_SETUP_CONFIG_PATH'] : env_config['path']
env_config['environment'] = ENV['ENVIRONMENT'] ? ENV['ENVIRONMENT'] : env_config['environment']
env_config['do_client_id'] = ENV['DO_CLIENT_ID'] ? ENV['DO_CLIENT_ID'] : env_config['do_client_id']
env_config['do_api_key'] = ENV['DO_API_KEY'] ? ENV['DO_API_KEY'] : env_config['do_api_key']

File.open(env_config_file, "w") do |file|
  file.write env_config.to_yaml
end

settings = YAML.load_file env_config['path'] + '/vagrant.yaml'

ipAddresses = settings['base']['ip-addresses']
projectName = 'secureelasticsearch'
environment = env_config['environment']
machineBaseName = settings['base']['basename'] ? settings['base']['basename'] : 'elasticsearch'
machineDomain = settings['base']['domain'] ? settings['base']['domain'] : 'example.com'

ipAddresses.each_with_index do |ipAddress, index|
    Vagrant.configure('2') do |config|
        config.vm.define "#{machineBaseName}-#{index}" do |elasticsearch|
            elasticsearch.vm.box      = settings['base']['box']
            elasticsearch.vm.hostname = "#{machineBaseName}-#{index}.#{machineDomain}"

            elasticsearch.vm.network :private_network, ip: ipAddress

            elasticsearch.vm.provider 'virtualbox' do |my_vm|
                my_vm.customize [
                    'modifyvm', :id,
                    '--memory', settings['base']['memory']
                ]
            end

            elasticsearch.vm.provider :digital_ocean do |provider, override|
                override.ssh.private_key_path = '~/.ssh/id_rsa'
                override.vm.box     = settings['digital-ocean']['box']
                override.vm.box_url = settings['digital-ocean']['box_url']

                provider.client_id  = env_config['do_client_id']
                provider.api_key    = env_config['do_api_key']

                provider.image      = settings['digital-ocean']['image']
                provider.region     = settings['digital-ocean']['region']
                provider.size       = settings['digital-ocean']['size']

                provider.private_networking = settings['digital-ocean']['private_networking']
                provider.backups_enabled    = settings['digital-ocean']['backups_enabled']
            end

            # Required so that private repos can be loaded by composer
            elasticsearch.ssh.forward_agent = true

            elasticsearch.vm.synced_folder '.', '/home/vagrant/' + projectName, :nfs => true
            elasticsearch.vm.synced_folder env_config['path'], '/etc/' + projectName, :nfs => true

            elasticsearch.vm.provision :shell do |shell|
                shell.path = 'scripts/bootstrap.sh'
                shell.args = "'#{environment}' '/home/vagrant/#{projectName}' '/root/.ssh/known_hosts'"
            end
        end
    end
end

